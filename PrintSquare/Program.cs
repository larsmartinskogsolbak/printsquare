﻿using System;

namespace PrintSquare
{
    class Program
    {
        static void Main(string[] args)
        {
        
            DrawSquare();
        }

        public static void DrawSquare()
        {
            Console.WriteLine("Enter width for a reactangle:");
            int width = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter height for a rectangle");
            int height = Convert.ToInt32(Console.ReadLine());

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Console.Write("#");
                }
                Console.WriteLine();
            }
        }

    }
}
